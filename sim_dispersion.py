#! /usr/bin/python
#
# Python script to simulate the effect of dispersion on a pulse.
# 

import numpy as np
from numpy import fft 
import pylab as plt

##
# Function for genrating a pulse. Now genrates a Gaussian pulse
# args: T0 half-widht of the pulse
# 		Ts sampling interval
#		N  # of sample points
# returns:
#		gauss_pulse the generated pulse
#		sample_pts the points where the sample was taken
def gen_pulse(T0,Ts,N):
	# The sample points will [-N*Ts/2 : Ts : N*Ts/2 ]
	sample_pts = np.arange(-(N*Ts)/2,(N*Ts)/2,Ts)
	
	gauss_pulse = np.exp(-sample_pts**2/(2*T0**2))

	return gauss_pulse, sample_pts

##
# Function to take FFT of the give pulse

def fft_pulse(pulse,N,norm=None):
	# w =  [ -pi:2*pi/N:pi) the last term will be not present
	w = fft.fftfreq(N,1/(2*np.pi))
	fft_pts = fft.fft(pulse,N,norm=norm)

	return fft_pts, w

##
#Funtion to calulate dispersion
def calc_dispersion(fft_pulse,h,beta2,w,N,Ts,norm=None):
	omega = w #/(Ts)
	f_disp = np.exp((1j*beta2*h*omega**2)/2)*fft_pulse
	dispersed_pulse = fft.ifft(f_disp,N,norm=norm)

	return dispersed_pulse

# Main
# Number of pts, sampling interval and gaussian pusle width
N=2**15
Ts=1e-12
T0=1e-9
beta2 = -10
h=100e3

pulse, t = gen_pulse(T0,Ts,N)

pulse_fft, w = fft_pulse(pulse,N)
pulse_d = calc_dispersion(pulse_fft,h,beta2,w,N,Ts)

# Now plot the result
plt.figure(1)
plt.plot(t, pulse, label = r'Gaussian Pulse')
plt.plot(t, np.absolute(pulse_d), label = r'Gaussian Pulse Dispersed')
plt.legend()
plt.grid()
#plt.figure(2)
#plt.plot(fft.fftshift(w)*(1/Ts), np.absolute(fft.fftshift(pulse_fft)), label = r'$Gaussian Pulse FFT$')
#plt.legend()
#plt.grid()
plt.show()
