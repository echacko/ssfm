# -*- coding: utf-8 -*-
"""
Created on Thu Sep  8 19:05:07 2016

@author: Ebin
"""

"""
This script will solve the NLS equation using split step Fourier method.
"""

import numpy as np
import pylab as plt
from numpy import fft

# from math import tanh


# Compute sech
#
# def sech(x):
#    return np.tanh(x)**-1

sech = lambda x: np.tanh(x) ** -1


##
# Function for generating a Gaussian pulse.
def gen_gauss_pulse(T0, C, Ts, N):
    # The sample points will [-N*Ts/2 : Ts : N*Ts/2 ]
    sample_pts = np.arange(-(N * Ts) / 2, (N * Ts) / 2, Ts)

    gauss_pulse = np.exp(-(1 + 1j * C) * sample_pts ** 2 / (2 * T0 ** 2))

    return gauss_pulse, sample_pts


##
# Function for genrating a super Gaussian pulse.
def gen_super_gauss_pulse(T0, C, m, Ts, N):
    # The sample points will [-N*Ts/2 : Ts : N*Ts/2 ]
    sample_pts = np.arange(-(N * Ts) / 2, (N * Ts) / 2, Ts)

    super_gauss_pulse = np.exp(-(1 + 1j * C) * sample_pts ** (2 * m) / (2 * T0 ** 2))

    return super_gauss_pulse, sample_pts


##
# Function for genrating a Gaussian pulse.
def gen_sech_pulse(T0, C, Ts, N):
    # The sample points will [-N*Ts/2 : Ts : N*Ts/2 ]
    sample_pts = np.arange(-(N * Ts) / 2, (N * Ts) / 2, Ts)

    sech_pulse = sech(sample_pts / T0) * np.exp(-1j * C * sample_pts ** 2 / (2 * T0 ** 2))

    return sech_pulse, sample_pts


##
# Function to take FFT of the give pulse
def fft_pulse(pulse, N, norm=None):
    # w =  [ -pi:2*pi/N:pi) the last term will be not present
    w = fft.fftfreq(N, 1 / (2 * np.pi))
    fft_pts = fft.fft(pulse, N, norm=norm)

    return fft_pts, w


##
# Function to calulate dispersion
def calc_dispersion(fft_pulse, h, beta2, omega, N, Ts, norm=None):
    f_disp = np.exp((1j * beta2 * h * omega ** 2) / 2) * fft_pulse
    dispersed_pulse = fft.ifft(f_disp, N, norm=norm)

    return dispersed_pulse


# Main
# Sampling paras
N = 2 ** 15  # No of FFT points
Ts = 1e-9  # Sampling interval

# Fiber paras
beta2 = -10  # Second order dispersion term
alpha = 1e-6  # Fiber attenuation factor
gamma = 10  # Nonlinear parameter
L = 10  # The total length of the fiber

# Input pulse paras
P0 = 4  # Input pulse power
T0 = 1e-6  # The pulse width
C = 0  # Freqency chirp. 0 for no chirp,<0 for down chirp and >0 upchirp.

h = 1e-2  # The stepsize

# The pulse we generate here will have peak power 1
pulse, t = gen_gauss_pulse(T0, C, Ts, N)

# Now normalize the time as T/T0
tau = t / T0

# Calculate the dipersion and nonlinear lenght
Ld = T0 ** 2 / abs(beta2)  # Dispersion lenght
Lnl = 1 / (gamma * P0)  # Nonlinear lenght

print("Ld = ", Ld, "Lnl = ", Lnl)

pulse_fft, omega = fft_pulse(pulse, N)

# The out pulse only if dispersion was there
pulse_d = calc_dispersion(pulse_fft, L, beta2, omega, N, Ts)

# There will a dispersion that needs to be multiplied to the pulse FFT
# This term will be multiplied in each step
exp_fft = np.exp((1j * beta2 * (h / 2) * omega ** 2) / 2)
# exp_fft = np.exp((1j*beta2*h*omega**2)/2)

# Initialize the pulse at 0
in_pulse = pulse

# Now start the loop
for count in range(1, int(L / h) + 1):
    disp_1_fft = exp_fft * fft.fft(in_pulse, N)
    disp_1 = fft.ifft(disp_1_fft, N)
    nonlinear = np.exp(1j * gamma * (np.absolute(disp_1) ** 2) * h) * disp_1
    disp_2_fft = exp_fft * fft.fft(nonlinear, N)
    disp_2 = fft.ifft(disp_2_fft, N)
    in_pulse = disp_2

# for count in range(1,int(L/h) + 1):
#    nonlinear_1 = np.exp(1j*gamma*(np.absolute(in_pulse)**2)*(h/2))*in_pulse
#    disp_fft = exp_fft*fft.fft(nonlinear_1,N)
#    disp =  fft.ifft(disp_fft,N)
#    nonlinear_2 = np.exp(1j*gamma*(np.absolute(disp)**2)*(h/2))*disp
#    in_pulse = disp

# Now we are out of the loop and so we now have the out pulse
out_pulse = in_pulse

# Now plot the result
plt.figure(1)
plt.plot(t, pulse, label=r'In Pulse')
plt.plot(t, np.absolute(pulse_d), label=r'Pulse Dispersed')
plt.plot(t, np.absolute(out_pulse), label=r'Pulse Out')
plt.legend()
plt.grid()
plt.show()
